from django.contrib import admin, messages
from django.db.models import QuerySet

from .models import *
from . import models


# Register your models here.





class InventoryFilter(admin.SimpleListFilter):
    title = 'inventroy'
    parameter_name = 'inventory'

    def lookups(self, request, model_admin):
        return [
            ('<10', 'Low')
        ]

    def queryset(self, request, queryset: QuerySet):
        if self.value() == '<10':
            return queryset.filter(inventory__lt=10)


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    autocomplete_fields = ['collection']
    prepopulated_fields = {
        'slug' : ['title']
    }
    actions = ['clear_inventory']

    list_display = ['title', 'price', ]
    list_editable = ['price']
    list_filter = ['collection', 'last_update', InventoryFilter]
    list_per_page = 10
    list_select_related = ['collection']


@admin.action(description='Clear inventory')
def clear_inventory(self, request, queryset=0):
    update_count = queryset.update(inventory=0)
    self.message_user(
        request,
        f'{update_count} products were successfully update',
        messages.success
    )

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'phone', 'membership']
    list_editable = ['membership', 'phone']
    ordering = ['first_name', 'last_name', 'membership']
    list_per_page = 10
    search_fields = ['first_name__istartswith', 'last_name__istartswith']

@admin.register(Collection)
class CollectionAdmin(admin.ModelAdmin):
    list_display = ['title','featured_product']
    search_fields = ['title']

    admin.site.register(Promotion)


class OrderItemInline(admin.TabularInline):
    autocomplete_fields = ['product']
    model= models.OrderItem

admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    autocomplete_fields = ['customer']
    inlines = [OrderItemInline]
    list_display = ['id','placed_at','customer']

admin.site.register(OrderItem)
admin.site.register(Address)
admin.site.register(Cart)
admin.site.register(CartItem)

