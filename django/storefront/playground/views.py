from django.shortcuts import render
from store.models import Customer

# Create your views here.
def say_hello(request):
    query_set = Customer.objects.filter(email__icontains='.com')
    for product in query_set:
        print(product)

    return render(request, 'hello.html',{'product':query_set,})