from django.contrib import admin
from store.admin import ProductAdmin
from django.contrib.contenttypes.admin import GenericTabularInline
from tags.models import Tag
from tags.models import TaggedItem


# Register your models here.
from store.models import Product


class TagInline(GenericTabularInline):
    autocomplete_fields = ['tag']
    model = TaggedItem


class CustomProductAdmin(ProductAdmin):
    inline = [TagInline]

admin.site.unregister(Product)
admin.site.register(Product,CustomProductAdmin)
