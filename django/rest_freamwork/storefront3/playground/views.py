# from django.core.mail import send_mail, mail_admins,BadHeaderError,EmailMessage
# from django.shortcuts import render
# from templated_mail.mail import BaseEmailMessage
#
#
# def say_hello(request):
#     try:
#         # message = EmailMessage('subject','message','from@suraj.com',['surajtembhare3@gmail.com'])
#         # message.attach_file('playground/static/images/Golden-Puppy_y15IkjY.png')
#         message = BaseEmailMessage(template_name='emails/hello.html', context={'name':'Suraj'})
#         message.send(['surajtembhare3@gmail.com'])
#     except BadHeaderError:
#         pass
#     return render(request, 'hello.html', {'name': 'Suraj'})


from django.core.cache import cache
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.views import APIView
import logging
import requests

logger = logging.getLogger(__name__)  # playground.views   #__name__


class HelloView(APIView):
    # @method_decorator(cache_page(5 * 60))
    def get(self, request):
        try:
            logger.info('Calling httpbin')
            response = requests.get('https://httpsbin.org/delay/2')
            logger.info('Received the response')
            data = response.json()
        except request.ConnectionError:
            print("No print statment")
            logger.critical('httpbin is offline')
        return render(request, 'hello.html', {'name': 'Suraj'})

