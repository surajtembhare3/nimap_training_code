from rest_framework.authtoken.admin import User
from django.contrib.auth.models import User
from rest_framework.test import APIClient
import  pytest

@pytest.fixture()
def authenticated(api_client):
    def do_authenticate(is_staff=False):
        return api_client.force_authenticate(user=User(is_staff=is_staff))
    return do_authenticate
