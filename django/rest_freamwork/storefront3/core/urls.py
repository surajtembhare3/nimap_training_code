from django.views.generic import TemplateView
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static


# URLConf
urlpatterns = [
    path('', TemplateView.as_view(template_name='core/index.html'))
]+ static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
