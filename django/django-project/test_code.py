import dns
from dns import resolver

result = dns.resolver.query('google.com', 'A')
for ipval in result:
    print('IP', ipval.to_text())
