import React, { Component } from 'react'
import './App.css';
import Modal from './components/Modal'
import axios from 'axios'


const tasks = [
{
    id:1,
    title: "Dunning",
    description: "Sending dunning letters to clients for uncollected cash.",
    completed: false
},
{
    id:2,
    title: "Order Release",
    description: "Check out customers accounts and release or block orders accordingly.",
    completed: true
},
{
    id:3,
    title: "Weekly Reports",
    description: "Sending the weekly reports for overdue invoices.",
    completed: false
},
{
    id:4,
    title: "Dunning",
    description: "Sending dunning letters to clients for uncollected cash.",
    completed: false
},
]
//function App() {
//  return (
//    <div className="App">
//      Hello!!
//    </div>
//  );
//}
class App extends Component {
    constructor(props){
        super(props);
        this.state= {
            modal: false,
            viewCompleted:false,

            activeItem: {
                title: "",
                description: "",
                completed: false
            },
            todoList : []
        };
    }
    componentDidMount(){
        this.refreshList();
    }

    refreshList = () => {
      axios.get("http://localhost:8000/api/tasks/").then(res => this.state({todoList : res.data})).catch(err => console.log(err)) //localhost:8000


    };


    //Create toggle property
    toggle = () => {
        this.setState({modal: !this.state.modal });
    };
    handleSubmit = item => {
        this.toggle();
//        alert('Saved!' + JSON.stringify(item));
         if (item.id){
            axios.put(`http://127.0.0.1:8000/api/tasks/${item.id}/`,item).then(res => this.refreshList(res => this.refreshList()))
         }
         axios.post("http://127.0.0.1:8000/api/tasks/", item).then(res => this.refreshList())
    };
    handleDelete = item => {
//        alert('Deleted!' + JSON.stringify(item));
      axios.delete(`http://127.0.0.1:8000/api/tasks/${item.id}/`,item).then(res => this.refreshList(res => this.refreshList()))
    };

    createItem = () => {
       const item = { title: "", modal: !this.state.modal };
       this.setState({ activeItem: item, modal: !this.state.modal });
    };

    editItem = item => {
        this.setState({ activeItem: item, modal: !this.state.modal })
    };

    displayCompleted = status => {
    if (status){
        return this.setState({ viewCompleted: true });
    }
    return this.setState({ viewCompleted: false });
    }

    renderTabList = () => {
        return(
            <div className="my-5 tab-list">
            <span
                onClick={() => this.displayCompleted(true)}
                className={this.state.viewCompleted ? "active" : ""}
                >
                Completed
                </span>
            <span
                onClick={() => this.displayCompleted(false)}
                className={this.state.viewCompleted ? "" : "active"}
                >
                InCompleted
                </span>
            </div>

        )
    }

// Rendring items in the list (completed || incompleted)
    renderItems = () => {
        const { viewCompleted } = this.state;
        const newItems = this.state.todoList.filter(
            item => item.completed === viewCompleted
        );


    return newItems.map(item =>(
    <li key={item.id}
        className="list-group-item d-flex justify-content-between align-items-center">
        <span className={`todo-title mr-2 ${this.state.viewCompleted ? "completed-todo" : ""}`}
            title={item.title}>

            {item.title}
            </span>
            <span>
            <button className="btn btn-info mr-2">Edit</button>

            <button className="btn btn-danger mr-2">Delete</button>
        </span>
    </li>

    ))

    };


    render() {
        return(
            <main className="context p-3 mb-2 bg-info">
                <h1 className ="text-white text-uppercase text-center my-4"> Task Manager</h1>
                <div className="row">
                <div className="col-md-6 col-sm-10 mx-auto p-0">
                <div className="card p-3">
                <div>
                <button className="btn btn-primary">Add Task</button>
                </div>
                {this.renderTabList()}
                <ul className="list-group list-group-flush">
                    {this.renderItems()}
                  </ul>
                </div>
                </div>
                </div>
                <footer className="my-3 mb-2 bg-info text-white text-center">Copyright 2022 &copy; All Rights Reserved</footer>
                {this.state.model ? (
                    <Modal activeItem={this.state.activeItem} toggle={this.toggle}
                    onSave={this.handleSubmit}  />
                ) : null}
            </main>
        )
    }
}



export default App;
